package com.api.reddit.controller;

import com.api.reddit.dto.CommentDto;
import com.api.reddit.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/comments/")
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<CommentDto> create(@RequestBody CommentDto commentDto) {
        return new ResponseEntity<>(commentService.save(commentDto), HttpStatus.CREATED);
    }

    @GetMapping("/by-post/{postId}")
    public ResponseEntity<List<CommentDto>> getAllcommentForPost(@PathVariable Long postId) {
        return new ResponseEntity(commentService.getAllCommentForPost(postId), HttpStatus.OK);
    }

    @GetMapping("/by-user/{username}")
    public ResponseEntity<List<CommentDto>> getAllcommentForUser(@PathVariable String username) {
        return new ResponseEntity(commentService.getAllCommentForUser(username), HttpStatus.OK);
    }

}
