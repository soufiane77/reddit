package com.api.reddit.service;

import com.api.reddit.dto.PostResponse;
import com.api.reddit.dto.RequestPost;
import com.api.reddit.exceptions.PostNotFoundException;
import com.api.reddit.exceptions.SpringRedditException;
import com.api.reddit.exceptions.SubredditNotFoundException;
import com.api.reddit.mapper.PostMapper;
import com.api.reddit.model.Post;
import com.api.reddit.model.Subreddit;
import com.api.reddit.model.User;
import com.api.reddit.repository.PostRepository;
import com.api.reddit.repository.SubredditRepository;
import com.api.reddit.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class Postservice {

    private final PostRepository postRepository;
    private final SubredditRepository subredditRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final PostMapper postMapper;

    public void save(RequestPost requestPost) {
        Subreddit subreddit = subredditRepository.findByName(requestPost.getSubredditName())
                .orElseThrow(() -> new SubredditNotFoundException(" subreddit non existe : "+requestPost.getSubredditName()));
        postRepository.save(postMapper.mapDtoToPost(requestPost, subreddit, authService.getCurrentUser()));
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAll() {
       return postRepository.findAll()
               .stream()
               .map(postMapper::mapPostToDto)
               .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public PostResponse getPostById(Long postId) {
        Post post =  postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException("aucun post avec l'id : "+postId));
        return postMapper.mapPostToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsBySubreddit(Long id) {
        Subreddit subreddit = subredditRepository.findById(id).orElseThrow(() -> new SubredditNotFoundException("subreddit n'existe pas avec l'id : "+id));
        return postRepository.findAllBySubreddit(subreddit).stream().map(postMapper::mapPostToDto).collect(Collectors.toList());
    }
    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByUsername(String username) {
        log.info("username : "+username);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new SpringRedditException("user n'existe pas avec le username: "));
        return postRepository.findByUser(user).stream().map(postMapper::mapPostToDto).collect(Collectors.toList());
    }
}
