package com.api.reddit.service;

import com.api.reddit.dto.CommentDto;
import com.api.reddit.exceptions.PostNotFoundException;
import com.api.reddit.exceptions.SpringRedditException;
import com.api.reddit.mapper.CommentMapper;
import com.api.reddit.model.Comment;
import com.api.reddit.model.Post;
import com.api.reddit.model.User;
import com.api.reddit.repository.CommentRepository;
import com.api.reddit.repository.PostRepository;
import com.api.reddit.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CommentService {

        private final CommentRepository commentRepository;
        private final PostRepository postRepository;
        private final AuthService authService;
        private final CommentMapper commentMapper;
        private final UserRepository userRepository;

        public CommentDto save(CommentDto commentDto) {
                Post post = postRepository.findById(commentDto.getPostId())
                        .orElseThrow(() -> new PostNotFoundException(commentDto.getPostId().toString()));
            Comment save = commentRepository.save(commentMapper.mapDtoToComment(commentDto, post, authService.getCurrentUser()));
            return commentMapper.mapCommentToDto(save);
        }

        public List<CommentDto> getAllCommentForPost(Long postId) {
                Post post = postRepository.findById(postId)
                        .orElseThrow(() -> new PostNotFoundException(postId.toString()));
                return commentRepository.findByPost(post)
                        .stream()
                        .map(commentMapper::mapCommentToDto)
                        .collect(Collectors.toList());
        }

        public List<CommentDto> getAllCommentForUser(String username) {
                User user = userRepository.findByUsername(username)
                        .orElseThrow(() -> new SpringRedditException(username));
                return commentRepository.findAllByUser(user)
                        .stream()
                        .map(commentMapper::mapCommentToDto)
                        .collect(Collectors.toList());
        }
}
