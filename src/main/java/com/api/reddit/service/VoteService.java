package com.api.reddit.service;

import com.api.reddit.dto.VoteDto;
import com.api.reddit.exceptions.PostNotFoundException;
import com.api.reddit.exceptions.SpringRedditException;
import com.api.reddit.mapper.VoteMapper;
import com.api.reddit.model.Post;
import com.api.reddit.model.Vote;
import com.api.reddit.repository.PostRepository;
import com.api.reddit.repository.VoteRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import static com.api.reddit.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
@Slf4j
public class VoteService {

    private final VoteRepository voteRepository;
    private final VoteMapper voteMapper;
    private final PostRepository postRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        Post post = postRepository.findById(voteDto.getPostId())
                .orElseThrow(() -> new PostNotFoundException("Post Not Found with ID - " + voteDto.getPostId()));

        Optional<Vote> voteByPostAndUser = voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post, authService.getCurrentUser());
        if (voteByPostAndUser.isPresent() &&
                voteByPostAndUser.get().getVoteType()
                        .equals(voteDto.getVoteType())) {
            log.info("voteByPostAndUser.get().getVoteType() = "+voteByPostAndUser.get().getVoteType());
            log.info("voteDto.getVoteType() = "+voteDto.getVoteType());
            log.info("voteDto.getDirection() = "+voteByPostAndUser.get().getVoteType().getDirection());
            throw new SpringRedditException("You have already "
                    + voteDto.getVoteType() + "'d for this post");
        }
        if (UPVOTE.equals(voteDto.getVoteType())) {
            post.setVoteCount(post.getVoteCount() + 1);
        } else {
            post.setVoteCount(post.getVoteCount() - 1);
        }
        voteRepository.save(voteMapper.mapDtoToVote(voteDto, post, authService.getCurrentUser()));
        postRepository.save(post);
    }
}
