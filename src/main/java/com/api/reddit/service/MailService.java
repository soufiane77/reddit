package com.api.reddit.service;

import com.api.reddit.exceptions.SpringRedditException;
import com.api.reddit.model.NotificationEmail;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.scheduling.annotation.Async;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender javaMailSender;
    private final MailContentBuilder mailContentBuilder;

    @Async
    public void sendMail(NotificationEmail notificationEmail) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("s.aqajjef@api.com");
            messageHelper.setTo(notificationEmail.getRecipient());
            messageHelper.setSubject(notificationEmail.getSubject());
            messageHelper.setText(mailContentBuilder.Build(notificationEmail.getBody()), true);
        };
         try {
             javaMailSender.send(messagePreparator);
             log.info("message d'activation envoyé");
         } catch (MailException e) {
             log.error("Erreur lors de l'envoi du mail", e);
             throw new SpringRedditException("Exception d'enoi de mail a " +notificationEmail.getRecipient(), e);
         }

    }
}
