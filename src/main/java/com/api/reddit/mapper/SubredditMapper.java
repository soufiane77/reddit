package com.api.reddit.mapper;

import com.api.reddit.dto.SubredditDto;
import com.api.reddit.model.Post;
import com.api.reddit.model.Subreddit;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubredditMapper {

    SubredditMapper INSTANCE = Mappers.getMapper(SubredditMapper.class);

    @Mapping(target = "numberPosts", expression = "java(mapPosts(subreddit.getPosts()))")
    SubredditDto mapSubredditToDto(Subreddit subreddit);

    default Integer mapPosts(List<Post> numberPosts) { return numberPosts.size(); };

    @InheritInverseConfiguration
    @Mapping(target = "posts", ignore = true)
    Subreddit mapDtoToSubreddit(SubredditDto subredditDto);

}
