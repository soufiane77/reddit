package com.api.reddit.mapper;

import com.api.reddit.dto.VoteDto;
import com.api.reddit.model.Post;
import com.api.reddit.model.User;
import com.api.reddit.model.Vote;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface VoteMapper {

    VoteMapper INSTANCE = Mappers.getMapper(VoteMapper.class);

    @Mapping(target = "voteType", expression = "java(voteDto.getVoteType())")
    @Mapping(target = "post", source = "post")
    @Mapping(target = "user", source = "user")
    Vote mapDtoToVote(VoteDto voteDto, Post post, User user);

    @Mapping(target = "voteType", expression = "java(vote.getVoteType())")
    @Mapping(target = "postId", expression = "java(vote.getPost().getPostId())")
    VoteDto mapVoteToDto(Vote vote);
}
