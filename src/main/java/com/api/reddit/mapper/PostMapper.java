package com.api.reddit.mapper;

import com.api.reddit.dto.PostResponse;
import com.api.reddit.dto.RequestPost;
import com.api.reddit.model.*;
import com.api.reddit.repository.CommentRepository;
import com.api.reddit.repository.VoteRepository;
import com.api.reddit.service.AuthService;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.github.marlonlom.utilities.timeago.TimeAgoMessages;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;
import java.util.Optional;

import static com.api.reddit.model.VoteType.DOWNVOTE;
import static com.api.reddit.model.VoteType.UPVOTE;

@Mapper(componentModel = "spring")
public abstract class PostMapper {

    @Autowired
    private  CommentRepository commentRepository;
    @Autowired
    private  VoteRepository voteRepository;
    @Autowired
    private AuthService authService;

    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "voteCount", constant = "0")
    @Mapping(target = "description", source = "requestPost.description")
    @Mapping(target = "subreddit", source = "subreddit")
    @Mapping(target = "user", source = "user")
    public abstract Post mapDtoToPost(RequestPost requestPost, Subreddit subreddit, User user);

    @Mapping(target = "id", source = "postId")
    @Mapping(target = "subredditName", source = "subreddit.name")
    @Mapping(target = "userName", source = "user.username")
    @Mapping(target = "commentCount", expression = "java(commentCount(post))")
    @Mapping(target = "duration", expression = "java(getDuration(post))")
    @Mapping(target = "upVote", expression = "java(isPostUpVoted(post))")
    @Mapping(target = "downVote", expression = "java(isPostDownVoted(post))")
    public abstract PostResponse mapPostToDto(Post post);

     Integer commentCount(Post post) {
        return commentRepository.findByPost(post).size();
    }

     String getDuration(Post post) {
         Locale LocaleBylanguageTag = Locale.forLanguageTag("en");
         TimeAgoMessages messages = new TimeAgoMessages.Builder().withLocale(LocaleBylanguageTag).build();
         return TimeAgo.using(post.getCreatedDate().toEpochMilli());
    }

     boolean isPostUpVoted(Post post) {
        return checkVoteType(post, UPVOTE);
    }

     boolean isPostDownVoted(Post post) {
        return checkVoteType(post, DOWNVOTE);
    }

    private boolean checkVoteType(Post post, VoteType voteType) {
        if (authService.isLoggedIn()) {
            Optional<Vote> voteForPostByUser =
                    voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post,
                            authService.getCurrentUser());
            return voteForPostByUser.filter(vote -> vote.getVoteType().equals(voteType))
                    .isPresent();
        }
        return false;
    }
}
